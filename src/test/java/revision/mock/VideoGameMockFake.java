package revision.mock;

public class VideoGameMockFake extends VideoGame {

	private int releaseYearMock;

	public VideoGameMockFake(int releaseYear) {
		super("", 0, "", "");
		this.releaseYearMock = releaseYear;
	}

	@Override
	public void play() {
	}

	@Override
	public int getPlayTime() {
		return 60;
	}

	@Override
	public int getReleaseYear() {
		return releaseYearMock;
	}

}
