package revision.mock;

public class VideoGameMockSpy extends VideoGame {

	public boolean hasBeenPlayed = false;
	public int howManyTimesWasPlayed = 0;

	public VideoGameMockSpy() {
		super("", 0, "", "");
	}

	@Override
	public void play() {
		hasBeenPlayed = true;
		howManyTimesWasPlayed += 1;
	}

	@Override
	public int getPlayTime() {
		return 0;
	}

	@Override
	public int getReleaseYear() {
		return 0;
	}

}
