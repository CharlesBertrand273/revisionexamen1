package revision.mock;

public class VideoGameMockStub extends VideoGame {

	public VideoGameMockStub() {
		super("", 0, "", "");
	}

	@Override
	public void play() {
	}

	@Override
	public int getPlayTime() {
		return 60;
	}

	@Override
	public int getReleaseYear() {
		return 2020;
	}

}
