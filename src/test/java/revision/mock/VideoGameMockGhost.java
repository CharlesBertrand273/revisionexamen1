package revision.mock;

/**
 * Le plus basique des mocks. Aucune logique, minimum viable, on l'utilise quand
 * il n'y aucune intraction avec le vrai objet dans la m�thode qu'on veut
 * tester.
 */
public class VideoGameMockGhost extends VideoGame {

	public VideoGameMockGhost() {
		super("", 0, "", "");
	}

	@Override
	public void play() {
	}

	@Override
	public int getPlayTime() {
		return 0;
	}

	@Override
	public int getReleaseYear() {
		return 0;
	}

}
