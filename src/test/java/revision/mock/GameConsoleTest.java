package revision.mock;

import java.util.ArrayList;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class GameConsoleTest {

	@Test
	public void when_addingVideoGameToCollection_then_newGameAvaiableInCollection() {
		VideoGameMockGhost ghost = new VideoGameMockGhost();
		GameConsole console = new GameConsole(new ArrayList<>());

		// Comme il n'y aucune interaction avec le jeu vid�o pour l'ajout, on a
		// simplement besoin
		// d'un mock pour ajouter dans la collection. Aucune interraction � tester, le
		// 'ghost' est donc
		// parfait pour la t�che!
		console.addNewGameToCollection(ghost);

		Assertions.assertEquals(ghost, console.selectGame(0));
	}

	@Test
	public void when_playingVideoGame_then_gameIsPlayedOnce() {
		VideoGameMockSpy spy = new VideoGameMockSpy();
		GameConsole console = new GameConsole(new ArrayList<>());
		console.addNewGameToCollection(spy);

		console.playGame(0);
		console.playGame(0);

		Assertions.assertTrue(spy.hasBeenPlayed);
		Assertions.assertEquals(2, spy.howManyTimesWasPlayed);
	}

}
