package revision.staticattributesandmethods;

public class Ferrari {

	public static final String trademark = "Ferrari";
	private static final long SERIAL_NUMBER_INCREMENT = 5L;

	private static long serialNumberGenerator = 115865L;

	private String model;
	private int releaseYear;
	private long serialNumber;

	public Ferrari(String model, int releaseYear) {
		this.model = model;
		this.releaseYear = releaseYear;
		this.serialNumber = getNextSerialNumber();
		updateSerialNumber();
	}

	public static long getNextSerialNumber() {
		return serialNumberGenerator + SERIAL_NUMBER_INCREMENT;
	}

	private static void updateSerialNumber() {
		serialNumberGenerator += SERIAL_NUMBER_INCREMENT;
	}

	public void drive() {
		// Conduit ta ferrari!
	}
}
