package revision.staticattributesandmethods;

public class MonzaAutodrome {

	public static void main(String[] args) {

		String trademark = Ferrari.trademark;
		long nextSerial = Ferrari.getNextSerialNumber();

		Ferrari testarossa = new Ferrari("Testarossa", 2011);

		testarossa.drive();

		long nextSerial2 = testarossa.getNextSerialNumber();

	}

}
