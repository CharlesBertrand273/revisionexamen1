package revision.mock;

public class VideoGame {

	private String title;
	private int releaseYear;
	private int playTime;
	private String maker;
	private String executable;

	public VideoGame(String title, int releaseYear, String maker, String executable) {
		super();
		this.title = title;
		this.releaseYear = releaseYear;
		this.maker = maker;
	}

	public void play() {
		Object[] args = (Object[]) new Object();
		System.console().format(executable, args);
	}

	public int getPlayTime() {
		return this.playTime;
	}

	public int getReleaseYear() {
		return releaseYear;
	}
}
