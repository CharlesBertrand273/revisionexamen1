package revision.mock;

import java.util.ArrayList;
import java.util.List;

public class GameConsole {

	public List<VideoGame> gamesAvailable;

	public GameConsole(List<VideoGame> gamesAvailable) {
		this.gamesAvailable = new ArrayList<>(gamesAvailable);
	}

	// Quel genre de mock a-t-on besoin pour cette m�thode?
	public void playGame(int index) {
		selectGame(index).play();
	}

	public VideoGame selectGame(int index) {
		return gamesAvailable.get(index);
	}

	// Quel genre de mock a-t-on besoin pour cette m�thode?
	public void addNewGameToCollection(VideoGame game) {
		this.gamesAvailable.add(game);
	}

	public void sortByReleaseYear() {
		gamesAvailable.sort(null);
	}
}
