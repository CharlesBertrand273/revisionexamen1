package revision.observer;

import java.util.ArrayList;
import java.util.List;

import revision.observer.abstraction.Observer;
import revision.observer.abstraction.Subject;

public class WednesdayClass implements Subject {
    
    private List<Student> students;
    private List<Observer> observers;
    private Director guest;

    public WednesdayClass(List<Student> students) {
        this.students = students;
        this.observers = new ArrayList<>();
    }

    public List<Student> getStudents() {
        return new ArrayList<>(students);
    }
    
    public void welcomeGuest(Director director) {
        this.guest = director;
        this.observers.forEach(o -> o.react("Director"));
        this.observers.add(director);
    }
    
    public void kickOutGuest() {
        this.observers.remove(this.guest);
        this.guest = null;
    }
    
    public void welcomeNewStudent(Student student) {
        this.students.add(student);
        this.observers.forEach(o -> o.react(student.getName()));
    }

    @Override
    public void addObserver(Observer o) {
        this.observers.add(o);
    }

    @Override
    public void removeObserver(Observer o) {
        this.observers.add(o);
    }

    @Override
    public void notifyAllObservers() {
        this.observers.forEach(o -> o.react());
    }
}
