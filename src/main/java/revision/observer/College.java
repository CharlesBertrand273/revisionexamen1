package revision.observer;

import java.util.ArrayList;
import java.util.List;

public class College {
    
    private List<Course> courses;

    public College() {
        this.courses = new ArrayList<>();
    }
    
    public College(List<Course> courses) {
        this.courses = courses;
    }
    
    public List<Course> getCourses() {
        return new ArrayList<>(courses);
    }
}
