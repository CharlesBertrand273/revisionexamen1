package revision.observer;

public class Course {
    
    private String title;
    private WednesdayClass classOfStudents;
    private ObjectOrientedProgTeacher teacher;
    
    public Course(String title, WednesdayClass classOfStudents, ObjectOrientedProgTeacher teacher) {
        this.title = title;
        this.classOfStudents = classOfStudents;
        this.teacher = teacher;
        classOfStudents.addObserver(teacher);
    }

    public String getTitle() {
        return title;
    }

    public WednesdayClass getClassOfStudents() {
        return classOfStudents;
    }
    
    public ObjectOrientedProgTeacher getTeacher() {
        return this.teacher;
    }
}
