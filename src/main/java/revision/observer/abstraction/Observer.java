package revision.observer.abstraction;

public interface Observer {
    
    void react();
    
    void react(String parameter);

}
