package revision.observer.abstraction;

public interface Subject {

    void addObserver(Observer o);
    
    void removeObserver(Observer o);
    
    void notifyAllObservers();
}
