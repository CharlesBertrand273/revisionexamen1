package revision.observer;

public class Student {
    
    private String matricule;
    private String name;
    
    public Student(String matricule, String name) {
        this.matricule = matricule;
        this.name = name;
    }

    public String getMatricule() {
        return matricule;
    }

    public String getName() {
        return name;
    }    
}
