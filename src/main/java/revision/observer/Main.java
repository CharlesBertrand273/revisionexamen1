package revision.observer;

import java.util.ArrayList;
import java.util.List;

public class Main {

    public static void main(String[] args) {
        Director director = new Director(1, "Boss");
        
        ObjectOrientedProgTeacher teacher1 = new ObjectOrientedProgTeacher(123, "Catherine");
        ObjectOrientedProgTeacher teacher2 = new ObjectOrientedProgTeacher(124, "Fran�ois");
        
        Student student1 = new Student("123", "Jean");
        Student student2 = new Student("124", "Jean-Pierre");
        Student student3 = new Student("125", "Jean-Luc");
        Student student4 = new Student("126", "Jean-Fran�ois");
        Student student5 = new Student("127", "Jean-Philippe");
        Student student6 = new Student("128", "Jean-Gabriel");
        
        WednesdayClass java = new WednesdayClass(new ArrayList<>(List.of(student1, student2, student3, student4, student5 ,student6)));
        WednesdayClass mobile = new WednesdayClass(new ArrayList<>(List.of(student1, student2, student3, student4, student5 ,student6)));
        
        Course poo2Course = new Course("Prog Objet", java, teacher1);
        Course mobileCourse = new Course("Prog Mobile", mobile, teacher2);
        
        Student newStudent1 =  new Student("129", "Jean-Michel");
        Student newStudent2 =  new Student("129", "Jean-Marc");
        Student newStudent3 =  new Student("130", "Jean-Louis");
        
        System.out.println("Jean-Michel enters class");
        java.welcomeNewStudent(newStudent1);
        System.out.println("Director enters class");
        java.welcomeGuest(director);
        System.out.println("Jean-Marc enters class");
        java.welcomeNewStudent(newStudent2);
        System.out.println("Director leaves class");
        java.kickOutGuest();
        System.out.println("Jean-Louis enters class");
        java.welcomeNewStudent(newStudent3);
        
    }
}
