package revision.observer;

import revision.observer.abstraction.Observer;

public class ObjectOrientedProgTeacher implements Observer {
    
    private int teacherId;
    private String name;
    
    public ObjectOrientedProgTeacher(int teacherId, String name) {
        this.teacherId = teacherId;
        this.name = name;
    }

    public int getTeacherId() {
        return teacherId;
    }

    public String getName() {
        return name;
    }

    @Override
    public void react() {
        System.out.println("Hello, student!");
        
    }

    @Override
    public void react(String parameter) {
        System.out.println("Hello, " + parameter);        
    }
}
