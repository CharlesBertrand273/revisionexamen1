package revision.observer;

import revision.observer.abstraction.Observer;

public class Director implements Observer {
   
    private int directorId;
    private String name;
    
    public Director(int directorId, String name) {
        this.directorId = directorId;
        this.name = name;
    }
    
    public int getTeacherId() {
        return directorId;
    }
    
    public String getName() {
        return name;
    }

    @Override
    public void react() {
        System.out.println("Hello, young man!");
        
    }

    @Override
    public void react(String parameter) {
        System.out.println("Hello, young man!");
    }  
}
