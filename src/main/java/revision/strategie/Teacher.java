package revision.strategie;

public interface Teacher {
    
    void joke();
    
    void teach();
    
    void answerQuestion();
    
    void correctExams();
    
    void correctTPs();
    
    void tortureStudents();
}
