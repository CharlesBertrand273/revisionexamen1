package revision.strategie;

public class College {

    public static void main(String[] args) {
        Teacher javaTeacher = new JavaTeacher();
        Teacher mobileTeacher = new MobileTeacher();
        
        Course javaCourse = new Course("Java Course", javaTeacher);
        Course mobileCourse = new Course("Mobile Course", mobileTeacher);
        
        javaCourse.replaceTeacher(mobileTeacher);
        mobileCourse.replaceTeacher(javaTeacher);
    }
}
