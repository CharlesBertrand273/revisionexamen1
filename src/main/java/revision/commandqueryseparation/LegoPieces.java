package revision.commandqueryseparation;

public class LegoPieces {

	private String name;
	private int number;
	private String color;

	public LegoPieces(String name, int number, String color) {
		super();
		this.name = name;
		this.number = number;
		this.color = color;
	}
}
