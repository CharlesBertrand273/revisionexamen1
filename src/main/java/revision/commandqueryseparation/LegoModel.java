package revision.commandqueryseparation;

import java.util.Map;

/**
 * Cette classe brise le Command-Query Separation Principle. Pourquoi et comment
 * le r�parer?
 * 
 * @author cboileau
 *
 */
public class LegoModel {

	private String name;
	private int number;
	private Map<LegoPieces, Integer> pieces;
	private int targetAge;
	private int numberOfStars;
	private int numberOfRaters;

	public LegoModel(String name, int number, Map<LegoPieces, Integer> pieces, int targetAge) {
		this.name = name;
		this.number = number;
		this.pieces = pieces;
		this.targetAge = targetAge;
		this.numberOfStars = 0;
		this.numberOfRaters = 0;
	}

	public void addPiece(LegoPieces piece) {
		if (this.pieces.containsKey(piece)) {
			int numberOfPieces = this.pieces.get(piece) + 1;
			this.pieces.put(piece, numberOfPieces);
		}
	}

	public int calculateRating(int rating) {
		this.numberOfRaters += 1;
		this.numberOfStars += rating;

		return this.numberOfStars / this.numberOfRaters;
	}

	public int calculateBuildingTime(int builderSkill) {
		return pieces.size() * (targetAge / builderSkill);
	}

}
