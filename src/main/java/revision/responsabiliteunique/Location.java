package revision.responsabiliteunique;

/**
 * Cette classe brise le principe de la responsabilité unique. Pourquoi et
 * comment réparer le bris?
 * 
 * @author cboileau
 *
 */
public class Location {

	private String clientName;
	private String roomName;
	private float standardPrice;
	private float couponCodeForPrice;
	private String creditCardNumber;
	private int numberOfPayments;

	public Location(String clientName, String roomName, float standardPrice, float couponCodeForPrice,
			String creditCardNumber, int numberOfPayments) {
		this.clientName = clientName;
		this.roomName = roomName;
		this.standardPrice = standardPrice;
		this.couponCodeForPrice = couponCodeForPrice;
		this.creditCardNumber = creditCardNumber;
		this.numberOfPayments = numberOfPayments;
	}

	public void rentRoom(float couponCode, String creditcardNumber) {
		float price = this.standardPrice * (couponCode / 100);
		CreditCard card = new CreditCard(clientName, creditCardNumber, 8, 2024, 111);
		CreditCardPayment payment = new CreditCardPayment();
		payment.approvePayment(card, price);
	}
}
