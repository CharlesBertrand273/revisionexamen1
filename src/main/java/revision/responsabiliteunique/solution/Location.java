package revision.responsabiliteunique.solution;

/**
 * Cette classe brise le principe de la responsabilit� unique. Pourquoi et
 * comment r�parer le bris?
 * 
 * @author cboileau
 *
 */
public class Location {

	/**
	 * Cette classe fait beaucoup de choses disparates. Id�alement, le calcul du
	 * prix se ferait dans une classe Billing et la location prendrait une chambre
	 * et la r�serverait.
	 */
	private String clientName;
	private String roomName;
	private float standardPrice;
	private float couponCodeForPrice;
	private String creditCardNumber;
	private int numberOfPayments;

	public Location(String clientName, String roomName, float standardPrice, float couponCodeForPrice,
			String creditCardNumber, int numberOfPayments) {
		this.clientName = clientName;
		this.roomName = roomName;
		this.standardPrice = standardPrice;
		this.couponCodeForPrice = couponCodeForPrice;
		this.creditCardNumber = creditCardNumber;
		this.numberOfPayments = numberOfPayments;
	}

	/**
	 * Ici, la m�thode s'occupe de faire la r�servation, cr�er la carte de cr�dit et
	 * cr�er le paiement. Chaque cr�ation peut �tre s�parer dans une factory pour
	 * aider � y voir plus clair.
	 * 
	 * @param couponCode
	 * @param creditcardNumber
	 */
	public void rentRoom(float couponCode, String creditcardNumber) {
		float price = this.standardPrice * (couponCode / 100);
		CreditCard card = new CreditCard(clientName, creditCardNumber, 8, 2024, 111);
		CreditCardPayment payment = new CreditCardPayment();
		payment.approvePayment(card, price);
	}
}
