package revision.responsabiliteunique.solution;

public class CreditCardPayment {

	public boolean approvePayment(CreditCard card, float amount) {
		if (card.isNotExpired() && card.isNotFull()) {
			return doesCreditCardEmitterApprovedPayment(amount);
		}

		return false;
	}

	private boolean doesCreditCardEmitterApprovedPayment(float amount) {
		return true;
	}

}
