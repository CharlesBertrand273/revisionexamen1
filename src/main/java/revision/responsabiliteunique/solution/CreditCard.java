package revision.responsabiliteunique.solution;

public class CreditCard {

	private String holder;
	private String number;
	private int expirationMonth;
	private int expirationYear;
	private int cvv;

	public CreditCard(String holder, String number, int expirationMonth, int expirationYear, int cvv) {
		this.holder = holder;
		this.number = number;
		this.expirationMonth = expirationMonth;
		this.expirationYear = expirationYear;
		this.cvv = cvv;
	}

	public boolean isNotExpired() {
		return true;
	}

	public boolean isNotFull() {
		return true;
	}
}
