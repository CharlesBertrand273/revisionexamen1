package revision.factory;

public class Lego {

    private int id;
    private String name;
    private LegoTheme theme;
    private int minimumAge;
    
    public Lego(int id, String name) {
        this.id = id;
        this.name = name;
        this.theme = LegoTheme.STAR_WARS;
        this.minimumAge = 6;
    }

    public Lego(int id, String name, LegoTheme theme, int minimumAge) {
        this.id = id;
        this.name = name;
        this.theme = theme;
        this.minimumAge = minimumAge;
    }

    public String getName() {
        return this.name;
    }

    public LegoTheme getTheme() {
        return this.theme;
    }

    public Integer getId() {
        return this.id;
    }
    
    public Integer getAge() {
        return this.minimumAge;
    }
}
