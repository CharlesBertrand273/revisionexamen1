package revision.factory;

public class LegoFactory {
    
    public Lego createLego(LegoTheme theme, int id, String name) {
        return switch(theme) {
            case STAR_WARS -> new Lego(id, name, theme, 6);
            case NINJAGO -> new Lego(id, name, theme, 7);
            case CITY -> new Lego(id, name, theme, 7);
            case ELVES -> new Lego(id, name, theme, 5);
            case HARRY_POTTER -> new Lego(id, name, theme, 8);
            default -> throw new IllegalArgumentException("Unexpected value: " + theme);
        };
    }

}
