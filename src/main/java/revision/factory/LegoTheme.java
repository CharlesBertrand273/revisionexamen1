package revision.factory;

public enum LegoTheme {
    
    STAR_WARS, HARRY_POTTER, NINJAGO, ELVES, CITY;

}
