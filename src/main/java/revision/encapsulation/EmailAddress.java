package revision.encapsulation;

/**
 * Cette classe brise le principe d'encapsulation. Comment et pourquoi? Que
 * peut-on faire pour fixer le bris d'encapsulation.
 * 
 * @author Catherine Boileau
 * @version 1.0
 */
public class EmailAddress {

	public String emailAddress;

	public EmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
	}
}
