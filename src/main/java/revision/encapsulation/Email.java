package revision.encapsulation;

import java.util.ArrayList;
import java.util.List;

/**
 * Cette classe brise le principe d'encapsulation. Comment et pourquoi? Que
 * peut-on faire pour fixer le bris d'encapsulation.
 * 
 * @author Catherine Boileau
 * @version 1.0
 */
public class Email {

	private static final int MAXIMUM_LENGTH_FOR_TEXT = 250;
	private static final int MAXIMUM_LENGTH_TITLE = 35;

	private String title;
	private List<EmailAddress> toEmailAddresses;
	private List<EmailAddress> ccEmailAddresses;
	private List<EmailAddress> bccEmailAddresses;
	private String text;

	public Email(String title, List<EmailAddress> toEmailAddresses, List<EmailAddress> ccEmailAddresses,
			List<EmailAddress> bccEmailAddresses, String text) {
		this.title = title;
		this.toEmailAddresses = new ArrayList<>(toEmailAddresses);
		this.ccEmailAddresses = new ArrayList<>(ccEmailAddresses);
		this.bccEmailAddresses = new ArrayList<>(bccEmailAddresses);
		this.text = text;
	}

	public List<EmailAddress> getToEmailAddresses() {
		return toEmailAddresses;
	}

	public List<EmailAddress> getCcEmailAddresses() {
		return ccEmailAddresses;
	}

	public List<EmailAddress> getBccEmailAddresses() {
		return bccEmailAddresses;
	}

	public void sendEmail(EmailServer server) {
		server.sendEmail(this);
	}

	public void writeEmail(String title, String text) {
		if (title.length() <= MAXIMUM_LENGTH_TITLE) {
			this.title = title;
		}

		if (text.length() <= MAXIMUM_LENGTH_FOR_TEXT) {
			this.text = text;
		}
	}
}
