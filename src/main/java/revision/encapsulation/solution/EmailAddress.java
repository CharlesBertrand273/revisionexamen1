package revision.encapsulation.solution;

/**
 * Cette classe brise le principe d'encapsulation. Comment et pourquoi? Que
 * peut-on faire pour fixer le bris d'encapsulation.
 * 
 * @author Catherine Boileau
 * @version 1.0
 */
public class EmailAddress {

	/**
	 * Avec un attribut 'public', l'encapsulation est bris� puisqu'on r�v�le � tous
	 * les objets comment EmailAddress est impl�ment�. Si, dans le futur, il faut
	 * modifier cette classe ou encore changer la valeur de emailAddress de String �
	 * autre chose, toutes les classes qui utilisent cet attribut directement
	 * devront changer, cr�ant ainsi une vague d'effets monstrueuse.
	 * 
	 * La fa�on de r�gler le probl�me est de mettre l'attribut priv� et d'attendre
	 * que ce soit n�cessaire avant de faire un get ou un set.
	 */
	public String emailAddress;

	public EmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
	}
}
