package revision.encapsulation.solution;

import java.util.ArrayList;
import java.util.List;

/**
 * Les m�thodes 'get' pour les 3 listes de destinataires brisent le principe
 * d'encapsulation, en retournant directement la r�f�rence de cette liste � un
 * objet de l'externe. Cet objet, qui r�cup�re la liste, peut la modifier sans
 * le consentement de la classe 'Email'.
 * 
 * C'est la fa�on la plus sournoise de briser le principe d'encapsulation, parce
 * qu'� premi�re vue, tout semble correct (attributs priv�s, pas de get/set
 * syst�matiques), mais c'est bel et bien un bris.
 * 
 * Une bonne fa�on de r�parer ce bris est de retourner une copie de ces listes,
 * ou encore de retourner une liste immuable (qu'on ne peut changer).
 * 
 * @author Catherine Boileau
 * @version 1.0
 */
public class Email {

	private String title;
	private List<EmailAddress> toEmailAddresses;
	private List<EmailAddress> ccEmailAddresses;
	private List<EmailAddress> bccEmailAddresses;
	private String text;

	public Email(String title, List<EmailAddress> toEmailAddresses, List<EmailAddress> ccEmailAddresses,
			List<EmailAddress> bccEmailAddresses, String text) {
		this.title = title;
		this.toEmailAddresses = toEmailAddresses;
		this.ccEmailAddresses = ccEmailAddresses;
		this.bccEmailAddresses = bccEmailAddresses;
		this.text = text;
	}

	public List<EmailAddress> getToEmailAddresses() {
		return new ArrayList<>(toEmailAddresses);
	}

	public List<EmailAddress> getCcEmailAddresses() {
		return ccEmailAddresses;
	}

	public List<EmailAddress> getBccEmailAddresses() {
		return bccEmailAddresses;
	}

	public void sendEmail(EmailServer server) {
		server.sendEmail(this);
	}

	public void writeEmail(String title, String text) {
		if (title.length() <= 35) {
			this.title = title;
		}

		if (text.length() <= 250) {
			this.text = text;
		}
	}

}
