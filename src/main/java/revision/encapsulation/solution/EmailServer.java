package revision.encapsulation.solution;

/**
 * Cette classe brise le principe d'encapsulation. Comment et pourquoi? Que
 * peut-on faire pour fixer le bris d'encapsulation.
 * 
 * @author Catherine Boileau
 * @version 1.0
 */
public class EmailServer {

	private String stmpServerUrl;
	private String stmpPort;
	private String username;
	private String password;

	public EmailServer(String stmpServerUrl, String stmpPort, String username, String password) {
		this.stmpServerUrl = stmpServerUrl;
		this.stmpPort = stmpPort;
		this.username = username;
		this.password = password;
	}

	/**
	 * Les get et les set syst�matiques sont aussi un bris d'encapsulation,
	 * puisqu'ils ne cachent pas grand-chose de l'impl�mentation de cet objet
	 * 'EmailServer'. C'est donc � ce niveau que se situe le bris d'encapsulation.
	 * 
	 * Pour le r�gler, il suffit d'enlever ces m�thodes et d'attendre qu'elles
	 * soient vraiment n�cessaires avant de les impl�menter. Bien souvent, les get
	 * sont utiles pour r�cup�rer les attributs lors de sauvegardes ou de conversion
	 * vers d'autres formats. mais les 'set' direct ne sont pas n�cessaires, si on
	 * respecte le 'Tell Don't Ask'.
	 * 
	 */
	public String getStmpServerUrl() {
		return stmpServerUrl;
	}

	public void setStmpServerUrl(String stmpServerUrl) {
		this.stmpServerUrl = stmpServerUrl;
	}

	public String getStmpPort() {
		return stmpPort;
	}

	public void setStmpPort(String stmpPort) {
		this.stmpPort = stmpPort;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public void sendEmail(Email email) {
		// Connect to smtp server using server url and port
		// Authenticate with username and password
		// Transform email to MIME type
		// Send MIME type
	}
}
