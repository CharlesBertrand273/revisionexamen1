package revision.encapsulation;

/**
 * Cette classe brise le principe d'encapsulation. Comment et pourquoi? Que
 * peut-on faire pour fixer le bris d'encapsulation.
 * 
 * @author Catherine Boileau
 * @version 1.0
 */
public class EmailServer {

	private String stmpServerUrl;
	private String stmpPort;
	private String username;
	private String password;

	public EmailServer(String stmpServerUrl, String stmpPort, String username, String password) {
		this.stmpServerUrl = stmpServerUrl;
		this.stmpPort = stmpPort;
		this.username = username;
		this.password = password;
	}

	public String getStmpServerUrl() {
		return stmpServerUrl;
	}

	public void setStmpServerUrl(String stmpServerUrl) {
		this.stmpServerUrl = stmpServerUrl;
	}

	public String getStmpPort() {
		return stmpPort;
	}

	public void setStmpPort(String stmpPort) {
		this.stmpPort = stmpPort;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public void sendEmail(Email email) {
		// Connect to smtp server using server url and port
		// Authenticate with username and password
		// Transform email to MIME type
		// Send MIME type
	}
}
