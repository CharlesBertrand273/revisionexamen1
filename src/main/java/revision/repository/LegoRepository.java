package revision.repository;

import java.util.List;

import revision.factory.LegoTheme;

public interface LegoRepository {
    
    Lego getById(int id);
    
    List<Lego> getAll();
    
    void searchByName(String name);
    
    void create(Lego lego);
    
    void delete(int id);

    void searchByTheme(LegoTheme theme);
}
