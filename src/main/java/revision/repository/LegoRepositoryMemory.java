package revision.repository;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import revision.factory.LegoTheme;

public class LegoRepositoryMemory implements LegoRepository {
    
    private Map<Integer, Lego> legos = new HashMap<>();

    @Override
    public Lego getById(int id) {
        return legos.get(id);
    }

    @Override
    public List<Lego> getAll() {
        return legos.values().stream().collect(Collectors.toList());
    }

    @Override
    public void searchByName(String name) {
        legos.values().stream()
        .filter(lego -> lego.getName().contains(name))
        .collect(Collectors.toList());
        
    }

    @Override
    public void searchByTheme(LegoTheme theme) {
        legos.values().stream()
        .filter(lego -> lego.getTheme().equals(theme))
        .collect(Collectors.toList());
        
    }

    @Override
    public void create(Lego lego) {
        // TODO Auto-generated method stub
        
    }

    @Override
    public void delete(int id) {
        // TODO Auto-generated method stub
        
    }

}
