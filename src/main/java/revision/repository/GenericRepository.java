package revision.repository;

import java.util.List;

public interface GenericRepository<T> {
    
    T getById(int id);
    
    List<T> getAll();
    
    void searchByName(String name);
    
    void create(T lego);
    
    void delete(int id);
}
