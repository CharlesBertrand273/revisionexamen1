package revision.interfacesegregationprinciple;

public interface Pilotable {

	void fly();

	void drive();

	void sail();

	void ride();
}
