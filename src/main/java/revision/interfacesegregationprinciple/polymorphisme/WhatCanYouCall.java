package revision.interfacesegregationprinciple.polymorphisme;

public class WhatCanYouCall {

	public static void main(String[] args) {
		Bike bike = new Bike();

		bike.ride();
		bike.fly();

		Plane plane = new Plane();

		plane.fly();
		// Plane n'impl�mente pas Ridable, alors plane.ride n'est pas possible.
		// L'auto-complete de votre IDE ne vous le donne m�me pas.
		//TODO: plane.ride();

		Flyable ovni1 = bike;
		Flyable ovni2 = plane;

		ovni1.fly();
		ovni2.fly();

		// Pas possible, puisque dans Flyable, la seule m�thode disponible est fly();
		//TODO: ovni1.sail();

		Ridable bike1 = new Bike();
		//TODO: Bike bike2 = bike1;

		Ridable ride1 = bike;
		// Pas possible, parce que Plane n'impl�mente pas Ridable.
		//TODO: Ridable ride2 = plane;

		Ridable ride3 = new Bike();
		// Pas possible d'instancier une interface (aucun constructeur disponible
		// puisqu'abstraite)
		//TODO: Ridable ride4 = new Ridable();

		Flyable ovni3 = new Plane();
		Flyable ovni4 = new Bike();
		// Pas possible d'instancier une interface (aucun constructeur disponible
		// puisqu'abstraite)
		//TODO: Flyable ovni5 = new Flyable();
	}
}
