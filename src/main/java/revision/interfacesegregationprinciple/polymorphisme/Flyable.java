package revision.interfacesegregationprinciple.polymorphisme;

public interface Flyable {

	void fly();

}
