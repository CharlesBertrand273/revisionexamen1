package revision.interfacesegregationprinciple.polymorphisme;

public class Bike implements Ridable, Flyable {

	@Override
	public void fly() {
		// if (rider == E.T.) {
		// fly();
		// }

	}

	@Override
	public void ride() {
		// ride bike normally

	}

}
