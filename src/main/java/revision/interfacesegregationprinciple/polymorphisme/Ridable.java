package revision.interfacesegregationprinciple.polymorphisme;

public interface Ridable {

	void ride();
}
