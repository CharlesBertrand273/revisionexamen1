package revision.interfacesegregationprinciple;

public class Bike implements Pilotable {

	@Override
	public void fly() {
		// cannot fly a bike
		// except if you are E.T.
	}

	@Override
	public void drive() {
		// cannot drive a bike
	}

	@Override
	public void sail() {
		// cannot sail a boat
	}

	@Override
	public void ride() {
		// ride a bike
	}
}
