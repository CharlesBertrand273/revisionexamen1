package revision.demetertelldontask;

import java.util.ArrayList;
import java.util.List;

/**
 * Cette classe brise la loi de D�m�ter et par extension, le Tell Don't Ask.
 * Pourquoi et comment r�parer ce bris?
 * 
 * @author cboileau
 *
 */
public class TaskCalendar {

	private List<Task> tasks;

	public TaskCalendar(List<Task> tasks) {
		this.tasks = tasks;
	}

	public List<Task> getAllCompletedTask() {

		List<Task> completedTasks = new ArrayList<>();

		for (Task t : tasks) {
			for (Step s : t.getSteps()) {
				if (s.isExpired()) {
					completedTasks.add(t);
				}
			}
		}

		return completedTasks;
	}

}
