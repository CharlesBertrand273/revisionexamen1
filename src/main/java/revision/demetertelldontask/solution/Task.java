package revision.demetertelldontask.solution;

import java.time.LocalDateTime;
import java.util.List;

import revision.demetertelldontask.Step;

public class Task {

	private String title;
	private List<Step> steps;
	private LocalDateTime dueDate;

	public Task(String title, List<Step> steps, LocalDateTime dueDate) {
		this.title = title;
		this.steps = steps;
		this.dueDate = dueDate;
	}

	public String getTitle() {
		return title;
	}

	public List<Step> getSteps() {
		return steps;
	}

	public LocalDateTime getDueDate() {
		return dueDate;
	}

	public boolean isCompleted() {
		/**
		 * Le code pour v�rifier si une des �tapes est compl�t� vient ici
		 */
		for (Step s : steps) {
			if (s.getDueDate().isAfter(LocalDateTime.now())) {
				return true;
			}
		}
		return false;
	}
}
