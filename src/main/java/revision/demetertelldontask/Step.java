package revision.demetertelldontask;

import java.time.LocalDateTime;

public class Step {

	private String title;
	private String text;
	private LocalDateTime creationDate;
	private LocalDateTime dueDate;
	private boolean isCompletionStep;

	public Step(String title, String text, LocalDateTime creationDate, LocalDateTime dueDate,
			boolean isCompletionStep) {
		this.title = title;
		this.text = text;
		this.creationDate = creationDate;
		this.dueDate = dueDate;
		this.isCompletionStep = isCompletionStep;
	}

	public String getTitle() {
		return title;
	}

	public String getText() {
		return text;
	}

	public LocalDateTime getCreationDate() {
		return creationDate;
	}

	public LocalDateTime getDueDate() {
		return dueDate;
	}

	public boolean isCompletionStep() {
		return isCompletionStep;
	}

	public boolean isExpired() {
		return dueDate.isAfter(LocalDateTime.now());
	}

}
