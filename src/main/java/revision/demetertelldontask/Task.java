package revision.demetertelldontask;

import java.time.LocalDateTime;
import java.util.List;

public class Task {

	private String title;
	private List<Step> steps;
	private LocalDateTime dueDate;

	public Task(String title, List<Step> steps, LocalDateTime dueDate) {
		this.title = title;
		this.steps = steps;
		this.dueDate = dueDate;
	}

	public String getTitle() {
		return title;
	}

	public List<Step> getSteps() {
		return steps;
	}

	public LocalDateTime getDueDate() {
		return dueDate;
	}
}
